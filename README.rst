LaTex UPPA
==========

.. image:: ./texUPPA_logo.png
  :width: 200
  :alt: texUPPA logo

Ce dépôt met à disposition des classes, styles ou thèmes en adéquation avec
la charte graphique de l'UPPA en 2014. Pour plus d'information sur la
charte, voir le site de la direction de la communication :
`http://communication.univ-pau.fr/live/charte-graphique`_.

L'ensemble du code est mis à disposition sous Latex project public licence
`http://latex-project.org/lppl/`_.

Commentaires, suggestions ou contributions sont les bienvenus.

Contenu
-------

Ci-après est détaillé le contenu des sous dossiers:

logos
    Contient les logos de l'UPPA

Ucolors
    Contient le package latex ``Ucolors.sty`` qui met à disposition les
    couleurs définies par la charte. Elles sont nommées Uxxx, par exemple ``Ubleu``
    pour le bleu.

themeBeamer
    Contient un thème en adéquation avec celui proposé par la direction de la
    communication pour faire une présentation avec la classe beamer. Le nom du
    thème est UPPA2014.

themeBeamer2020
    Contient un thème en adéquation avec celui proposé par la direction de la
    communication pour faire une présentation avec la classe beamer. Le nom du
    thème est UPPA2020.

exemples
    Contient des exemples d'utilisations des diverses classes ou package latex.
	
thesuppa
    Contient la classe thesuppa.cls pour la rédaction d'une thèse à l'UPPA.

TUL
    La classe thesul.cls `http://www.loria.fr/~roegel/TeX/TUL.html`_ est une
    classe pour la rédaction d'une thèse. La classe thesuppa.cls est construite
    à partir de la classe thesul qui doit donc être disponnible.

cls
    Contient des classes ou package de base.
	
posterA0
	Contient un package python pour la création d'un poster.


Installation
------------

Il est recommandé de télécharger l'archive complète ce qui évitera des
problèmes de dépendances. Il suffit ensuite de l'extraire dans un dossier
accessible à LaTeX.

Sous linux, extraire dans ::

    $HOME/texmf/tex/latex/

Sous MacOS, extraire dans ::

    $HOME/Library/texmf/tex/latex/
    
Sous windows il faut utiliser les fonctionnalités de MikTex ou de la
distribution de LaTeX que vous avez installée.


Utilisation
-----------

Les classes et packages proposés ici s'utilisent comme tout autre package LaTeX
via les commandes ``\documentclass`` pour les classes et ``\usepackage`` pour
les packages. Le thème beamer UPPA2014 s'utilise via la commande
``\usetheme{UPPA2014}`` (voir beamerExemple pour plus de détails).

La classe thesuppa.cls hérite de la classe thesul.cls et dispose donc des mêmes
fonctionnalités.

