%% artuppa.cls
%% Copyright 2014 by Germain Vallverdu
%
% Classe article UPPA
%
% babel language have to be given with class arguments
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{artuppa}[17/10/2012, modele pour la classe article V1.0]

% default options
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax

% load base class
\LoadClass[a4paper, 11pt]{article}

% babel
\RequirePackage{babel}

\AtBeginDocument{\maketitle}

% -----------------------------------------------------------------------------
% Standard packages
% -----------------------------------------------------------------------------
\RequirePackage{xspace}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{amssymb}
\RequirePackage{graphicx}
\RequirePackage{float}
\RequirePackage{array}

% -----------------------------------------------------------------------------
% UPPA packages
% -----------------------------------------------------------------------------
\RequirePackage{UPPAheader}
\RequirePackage{UPPAstyle}

% default header
\HeaderUPPA

\endinput
